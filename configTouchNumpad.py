#!/usr/bin/env python
import sys
verStr = "3" if (sys.version_info[0] >= 3 ) else ""

try:
    import evdev 
except (ImportError  ) as e: 
    print( e)
    print("\n\n Please install python"+ verStr + "-evdev module. i.e. On Debian/Ubuntu:  sudo apt-get install python" + verStr + "-evdev" )
    exit();

from evdev import UInput, ecodes as e

import json

raw_input3 = vars(__builtins__).get('raw_input',input)


devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
i=0
for device in devices:
    print ("%d : %s\t %s\t %s"%(i,device.path, device.name, device.phys))
    i = i+1
    
if(i==0): 
        print("\nNo Devices found - Please run as root\n")        
        exit()
        
devIndex = int(raw_input3("Choose your touchpad device:"))


deviceFn = devices[devIndex].path
deviceName = devices[devIndex].name

device = evdev.InputDevice(deviceFn)
print(device)
caps = device.capabilities(absinfo=True)

#are these constant?
qminX = caps[3][0][1][1]
qmaxX = caps[3][0][1][2]
qminY = caps[3][1][1][1]
qmaxY = caps[3][1][1][2]

print (caps)

minX = int(raw_input3("MinX [%d]: "%(qminX)) or qminX)
maxX = int(raw_input3("MaxX [%d]: "%(qmaxX)) or qmaxX)
minY = int(raw_input3("MinY [%d]: "%(qminY)) or qminY)
maxY = int(raw_input3("MaxY [%d]: "%(qmaxY)) or qmaxY)




print ("Now specify how to divide up the touchpad:")
cols = int(raw_input3("Columns [%d]: "%(6)) or 6)
rows = int(raw_input3("Rows [%d]: "%(5)) or 5)


print ("WRITING SIMPLE NUMERIC KEYPAD MAPPING FOR 6x5 GRID")
print ("PLEASE EDIT CONFIG FILE TO CHANGE. REQUIRED IF MORE THAN 30 KEYS NEEDED")
data = {'device': "%s"%(deviceName) , 
		'minX': minX,
		'maxX': maxX,
		'minY': minY,
		'maxY': maxY,
		'rows': rows,
		'cols': cols,
		"keys": 
		[
		  "KEY_A" ,       "KEY_NUMLOCK",  "KEY_KPSLASH",  "KEY_KPASTERISK",  "KEY_BACKSPACE", "KEY_A", 
          "KEY_HOME",      "KEY_KP7",      "KEY_KP8",      "KEY_KP9",         "KEY_KPMINUS",   "KEY_A", 
          "KEY_END",       "KEY_KP4",      "KEY_KP5",      "KEY_KP6",         "KEY_KPPLUS",    "KEY_A", 
          "KEY_PAGEUP",    "KEY_KP1",      "KEY_KP2",      "KEY_KP3",         "KEY_ENTER",     "KEY_A", 
          "KEY_PAGEDOWN",  "KEY_KP0",      "KEY_KPCOMMA",  "KEY_KPDOT",       "KEY_ENTER",     "KEY_A"
		]
		} 
		
defOutFile = 'touchNumpad.json'
outfilename = raw_input3("write to config file named [%s]: "%(defOutFile)) or defOutFile
		
with open(outfilename, 'w') as outfile:
    json.dump(data, outfile)

