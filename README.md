TouchNumpad - turn your Linux laptop touchpad into an extra keyboard.
====================================================================

TouchNumpad ( via evDev) injects keypresses based on touchpad inputs. Useful for laptops without a numeric keypad when using programs such as Blender.

As of writing this runs under Python 3 or 2.

The main program is touchNumpad.py but a configuration needs to be created first using one or both of the following:
* **configTouchNumpad.py** 
    - will ask you to choose the touchpad device and will create a default config file.
* **gconfigTouchNumpad.py**
    - create/edit a config file - select device and assign keycodes.  
    - Print out HTML key template to place over your touchpad.

gconfigTouchNumpad does not yet allow you to change the number of rows and columns. Use configTouchNumpad first if 5x5 isn't suitable.

> Note:
> If you do not see any devices, ensure that your user is in the correct group (typically input) to have read/write access. Optional - manually insert device line into touchNumpad.json file  i.e. "device": "SynPS/2 Synaptics TouchPad" as shown in 'sudo xinput'.




* **touchNumpad.py** 
    - leave this running to turn your touchpad into a keypad. 
    - look at the code to verify it is safe to run as root. It's separate from the config and small for this reason.
    - run as root: sudo python3 touchNumpad.py
    - Without root: To get acces to /dev/uinput you need to write a udev permission and potentially modify AppArmour settings as well. This is beyond the scope of this readme.



KeyCodes:
If the file "input-event-codes.h" is in the local directory the file will be parsed for its keycodes.
"input-event-codes.h may be found in /usr/include/linux/ and should be used as-is. It is installed via the linux-libc-dev package.
If the file is not present you will be presented with subset of KEY keycodes. "KEY" will be the only entry under "types".
