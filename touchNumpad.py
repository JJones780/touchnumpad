#!/usr/bin/env python

import sys
verStr = "3" if (sys.version_info[0] >= 3 ) else ""

try:
    import evdev 
except (ImportError  ) as e: 
    print( e)
    print("\n\n Please install python"+ verStr + "-evdev module. i.e. On Debian/Ubuntu:  sudo apt-get install python" + verStr + "-evdev" )
    exit();

from evdev import UInput, ecodes as e

import json

raw_input3 = vars(__builtins__).get('raw_input',input)

try:
    with open('touchNumpad.json') as json_data_file:
        cfg = json.load(json_data_file)
except (OSError,IOError  ) as e:    
    print (e);
    print ("Problem reading config file. Run    sudo python configTouchNumpad.py   to generate one.")  
    print ("note: gconfigTouchNumpad.py will help you edit it")
    exit()    
#print(cfg)

devIndex = -1

devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
i=0
for device in devices:
    if(device.name == cfg["device"]): 
        devIndex = i
        break
    i = i+1



if devIndex == -1 :  #not found let user choose manually:
    i=0
    for device in devices:
        print ("%d : %s\t %s\t %s"%(i,device.path, device.name, device.phys))
        i = i+1
        
    print("Config File - No match for device:%s\nPlease re-run configuration utility "%(cfg["device"]))   
    if(i==0): 
        print("\nARE YOU RUNNING AS ROOT?\n")        
        exit()
    devIndex = int(raw_input3("or Choose your touchpad device:"))


device = evdev.InputDevice(devices[devIndex].path)

print(device)
device.capabilities()
device.grab()  #takeover touchpad i.e. no longer controls pointer if it had been



ui = UInput()

#touchpad min and max values:
minX = cfg['minX'] 
maxX = cfg['maxX'] 
minY = cfg['minY'] 
maxY = cfg['maxY'] 

#  "minX": 1360,
#  "maxX": 5700,
#  "minY": 1150,
#  "maxY": 4600,


#calc deltas
dX = maxX - minX
dY = maxY - minY

#map to grid of:
divX = cfg['cols'] #6
divY = cfg['rows'] #5

#Characters to insert for virtual keys:
# see linux/input.h  or  linux/input-event-codes.h ( on github etc. )
keycodes = cfg['keys']

#"keys": 
#  [
#		  "KEY_A" ,       "KEY_NUMLOCK",  "KEY_KPSLASH",  "KEY_KPASTERISK",  "KEY_BACKSPACE", "KEY_A", 
#          "KEY_HOME",      "KEY_KP7",      "KEY_KP8",      "KEY_KP9",         "KEY_KPMINUS",   "KEY_A", 
#          "KEY_END",       "KEY_KP4",      "KEY_KP5",      "KEY_KP6",         "KEY_KPPLUS",    "KEY_A", 
#          "KEY_PAGEUP",    "KEY_KP1",      "KEY_KP2",      "KEY_KP3",         "KEY_ENTER",     "KEY_A", 
#          "KEY_PAGEDOWN",  "KEY_KP0",      "KEY_KPCOMMA",  "KEY_KPDOT",       "KEY_ENTER",     "KEY_A"
#  ]

#make sure there are enough definitions!
while  len(keycodes) < (divX * divY):
	keycodes.append("KEY_X")
	

lastX = -1
lastY = -1
lastK = -1  #last down keycode
keyPressed = False
release = False

debug = False 

for event in device.read_loop():
    release = False
    if event.type == e.EV_ABS:
        if (event.code == 0 ): #abs x
            lastX = event.value
        else:
            if (event.code == 1 ): #abs Y
                lastY = event.value
            else:
                if (event.code == 24 ): # pressure event
                    release = event.value == 0  # released key
                    
                    kX = max(0, min(int( divX * (lastX - minX) /dX), divX )) 	#limit in case touchpad min and max improperly defined
                    kY = max(0, min(int( divY * (lastY - minY) /dY), divY ))
                    pressedKey = kX + divX*kY
                    #print("X=%d \t Y=%d \t divX=%d \tpressedKey=%d"%(kX,kY, divX, pressedKey))
                    
                    
                    if debug:
                        print ("############ X: %d \t Y:%d" % (lastX,lastY)) 
                        print(evdev.categorize(event))
                        print(event)
                        print ("-------- key col: %d \t  row: %d " % ( kX, kY))


                    if release:
                        ui.write(e.EV_KEY, e.ecodes[ keycodes[lastK]], 0)  # up
                        keyPressed = False
                        pressedKey = -1
                        lastK = -1
                        ui.syn()                
                        
                    else: 
                        if  (lastK != pressedKey ):  #new key hit
                            if keyPressed: #release old key
                                ui.write(e.EV_KEY, e.ecodes[ keycodes[lastK]], 0)  # up
                                keyPressed = False
                                ui.syn()

                            ui.write(e.EV_KEY, e.ecodes[ keycodes[pressedKey]], 1)  # down
                            keyPressed = True
                            lastK = pressedKey
                            ui.syn()
