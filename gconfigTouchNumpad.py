#!/usr/bin/env python

#TODO  Complain if quitting without save

try:
    import Tkinter as tk
    #import tkMessageBox
    import tkFileDialog
except:
    import tkinter as tk    
    #from tkinter import messagebox as tkMessageBox
    from tkinter import filedialog as tkFileDialog
    
try:
    import evdev 
except (ImportError  ) as e: 
    print( e)
    print("\n\n Please install python"+ verStr + "-evdev module. i.e. On Debian/Ubuntu:  sudo apt-get install python" + verStr + "-evdev" )
    exit();

from evdev import UInput, ecodes as e


import json
import re    

import argparse

cfg={}
initialSaveButtonState = tk.DISABLED
keys={}

parser = argparse.ArgumentParser()
parser.add_argument("filename", default='touchNumpad.json', nargs='?', help="specify  a config file")

args = parser.parse_args()



print (args)
#Read config file if it exists
try:
    with open(args.filename) as json_data_file:
        cfg = json.load(json_data_file)
except:        
    if (cfg == {}):
        cfg = {'device': "Please Choose a Device" , 
		'minX': 0,
		'maxX': 1000,
		'minY': 0,
		'maxY': 1000,
		'rows': 5,
		'cols': 5,
		"keys": 
		[
		  "KEY_A" ,       "KEY_NUMLOCK",  "KEY_KPSLASH",  "KEY_KPASTERISK",  "KEY_BACKSPACE", 
          "KEY_HOME",      "KEY_KP7",      "KEY_KP8",      "KEY_KP9",         "KEY_KPMINUS",   
          "KEY_END",       "KEY_KP4",      "KEY_KP5",      "KEY_KP6",         "KEY_KPPLUS",    
          "KEY_PAGEUP",    "KEY_KP1",      "KEY_KP2",      "KEY_KP3",         "KEY_ENTER",     
          "KEY_PAGEDOWN",  "KEY_KP0",      "KEY_KPCOMMA",  "KEY_KPDOT",       "KEY_ENTER"
		]
		}
        initialSaveButtonState = tk.NORMAL
    pass

#    name=tkFileDialog.askopenfilename(initialdir = "./",title = "%s not found. Choose config file"%(args.filename),filetypes = (("config files","*.json"),("all files","*.*")))
#    print (name)
#    try:
#        with open(name) as json_data_file:
#            cfg = json.load(json_data_file)
#            print(cfg)
#    except:
#        pass




# Read key file:
def parseKeys(fileName):
    try:  
        with open(fileName) as f:
            def parseLine(line):
                m = re.match(pttrn, line)
                if m:
                    return m.groups()
            pttrn = re.compile(r'\s*#define\s+(\w+?)_(\w+)')        
            for line in f:
                vals = parseLine(line)
                if vals:
                    if vals[0] not in keys:
                        keys[vals[0]] = []
                    keys[vals[0]].append(vals[1])
    except:
            print ('input-event-codes.h not found. Using smaller list of key options.')
            keys['KEY']=['KP0','KP1','KP2','KP3','KP4','KP5','KP6','KP7','KP8','KP9','NUMLOCK','KPSLASH','KPASTERISK','BACKSPACE','HOME','KPMINUS','END','KPPLUS','PAGEUP','ENTER','PAGEDOWN','KPCOMMA','KPDOT','ENTER','ESC','KPLEFTPAREN','KPRIGHTPAREN', 'UP', 'LEFT', 'RIGHT', 'DOWN', 'INSERT', 'DELETE', 'NEXTSONG', 'PLAYPAUSE', 'PREVIOUSSONG', 'CAMERA_ZOOMIN', 'CAMERA_ZOOMOUT', 'CAMERA_UP', 'CAMERA_DOWN', 'CAMERA_LEFT', 'CAMERA_RIGHT','RIGHT_UP', 'RIGHT_DOWN', 'LEFT_UP', 'LEFT_DOWN', 'ROOT_MENU', 'MEDIA_TOP_MENU', 'CHANNELUP', 'CHANNELDOWN', 'FIRST', 'LAST', 'AB', 'NEXT', 'RESTART', 'SLOW', 'SHUFFLE', 'BREAK', 'PREVIOUS', 'DIGITS', 'TEEN', 'TWEN']
            

parseKeys('input-event-codes.h')



# DND Drag and Drop from https://github.com/python/cpython/blob/master/Lib/tkinter/dnd.py
# The factory function

def dnd_start(source, event):
    h = DndHandler(source, event)
    if h.root:
        return h
    else:
        return None


# The class that does the work

class DndHandler:

    root = None

    def __init__(self, source, event):
        if event.num > 5:
            return
        root = event.widget._root()
        try:
            root.__dnd
            return # Don't start recursive dnd
        except AttributeError:
            root.__dnd = self
            self.root = root
        self.source = source
        self.target = None
        self.initial_button = button = event.num
        self.initial_widget = widget = event.widget
        self.release_pattern = "<B%d-ButtonRelease-%d>" % (button, button)
        self.save_cursor = widget['cursor'] or ""
        widget.bind(self.release_pattern, self.on_release)
        widget.bind("<Motion>", self.on_motion)
        widget['cursor'] = "hand2"

    def __del__(self):
        root = self.root
        self.root = None
        if root:
            try:
                del root.__dnd
            except AttributeError:
                pass

    def on_motion(self, event):
        x, y = event.x_root, event.y_root
        target_widget = self.initial_widget.winfo_containing(x, y)
        source = self.source
        new_target = None
        while target_widget:
            try:
                attr = target_widget.dnd_accept
            except AttributeError:
                pass
            else:
                new_target = attr(source, event)
                if new_target:
                    break
            target_widget = target_widget.master
        old_target = self.target
        if old_target is new_target:
            if old_target:
                old_target.dnd_motion(source, event)
        else:
            if old_target:
                self.target = None
                old_target.dnd_leave(source, event)
            if new_target:
                new_target.dnd_enter(source, event)
                self.target = new_target

    def on_release(self, event):
        self.finish(event, 1)

    def cancel(self, event=None):
        self.finish(event, 0)

    def finish(self, event, commit=0):
        target = self.target
        source = self.source
        widget = self.initial_widget
        root = self.root
        try:
            del root.__dnd
            self.initial_widget.unbind(self.release_pattern)
            self.initial_widget.unbind("<Motion>")
            widget['cursor'] = self.save_cursor
            self.target = self.source = self.initial_widget = self.root = None
            if target:
                if commit:
                    target.dnd_commit(source, event)
                else:
                    target.dnd_leave(source, event)
        finally:
            source.dnd_end(target, event)
            
#==============================================================================
#             
# Button Class which accepts dnd drops:
#             
#==============================================================================
class GButton(tk.Button):
    def dnd_accept(self, source, event):
        return self

    def dnd_enter(self, source, event):
        self.config(relief=tk.SUNKEN)

    def dnd_motion(self, source, event):
        pass

    def dnd_leave(self, source, event):
        self.config(relief=tk.RAISED)

    def dnd_commit(self, source, event):
        if( source.widget != 0 ): # swapping values
            source.widget['text'] = self.cget('text')
        if( self != source.widget):
            print(source)
            self.config(text=source.dnd)
            self.config(relief=tk.RAISED)
        
        
        
class DropItem():
        def __init__(self, value, source,callback):
            self.dnd = value
            self.widget = source
            self.endCallback = callback
        def dnd_end(self, source, event):
            if (self.endCallback):
                self.endCallback()
            return self
            
#==============================================================================
# #==============================================================================
# #    
# #  Listbox which provides keycodes for dnd
# #      
# #==============================================================================
# class KListbox(tk.Listbox):
# #    def __init__(self, parent, *args, **kwargs):
# #         tk.Listbox.__init__(self, parent)
# #         self.bind("<Button-1>", self.list_click )
#     def dnd_end(self, source, event):
#         return self
# 
# #    def list_click(event):
# #        w = event.widget
# #        index = w.nearest(event.y)
# #        w._selection = index
# #        print("grab release")
# #        w.grab_release()
#==============================================================================
            
            

class Main(tk.Frame):   
               
               
    def __init__(self, parent,cfg):
        tk.Frame.__init__(self, parent)
        
        self.root = parent
        
        self.calcWindowTitle()

        self.gridButtons = {}


        self.TopFrame = tk.Frame()
        self.TopFrame.pack(side=tk.TOP, fill=tk.X, padx=5, pady=5)
        
       # Dropdown menu options 
        devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
        
        if(devices.count == 0): 
            print("\nNo Devices found - Please add user to group: input   or run as root\n")        
            exit()



        # datatype of menu text 
        clicked = tk.StringVar() 
        
        # initial menu text 

        clicked.set( cfg['device'])

        def onSelectDevice(evDevice):
            clicked.set(evDevice.name)
            device = evdev.InputDevice(evDevice.path)
            print(device)
            caps = device.capabilities(absinfo=True)
            self.docChanged()

            try:
                #are these constant?
                cfg["minX"] = caps[3][0][1][1]
                cfg["maxX"] = caps[3][0][1][2]
                cfg["minY"] = caps[3][1][1][1]
                cfg["maxY"] = caps[3][1][1][2]

                print (caps)
            except:
                clicked.set("Inappropriate Choice :" + evDevice.name)
            cfg["device"] = evDevice.name
                        
        # Create Dropdown menu 
        drop = tk.OptionMenu( self.TopFrame , clicked , *devices , command=onSelectDevice) 
        drop.pack() 


        
        self.BGFrame = tk.Frame()  
        self.BGFrame.pack(side=tk.LEFT, fill=tk.X, padx=5, pady=5)

        pttrn = re.compile(r'\s*(\w+?)_(\w+)\s*')
        
        # Create Button Grid -fill based on cfg
        for i in range(0,cfg['rows']):
          self.gridButtons[i]=[]  
          for j in range(0,cfg['cols']):
               code = cfg['keys'][j + i *cfg['cols']]
               m = re.match(pttrn, code)
               
               b = GButton(self.BGFrame, text = m.group(1)+"\n"+m.group(2), width=8, height=4)
               b.grid(row=i,  column= j, sticky=tk.N+tk.E+tk.S+tk.W )
               b.bind("<Button-1>", self.on_select_gridbutton )
               self.gridButtons[i].append(b)
               
               
               
        self.keyframe = tk.Frame(width=15)
        self.keyframe.pack(side=tk.RIGHT, fill=tk.Y)
        
        self.label_widget = tk.Label(self.keyframe, text="TYPES")
        self.label_widget.pack(side=tk.TOP)
        
        #create a listbox
        self.listtypes = tk.Listbox(self.keyframe, height=5)
        self.listtypes.pack(side=tk.TOP)
        
        
        self.label_widget = tk.Label(self.keyframe, text="BUTTONS (Drag to position)")
        self.label_widget.pack()
        
        #create a listbox
        self.listbuttons = tk.Listbox(self.keyframe, selectmode=tk.SINGLE)
        self.listbuttons.pack( fill=tk.BOTH, expand=1)
        self.listbuttons.bind("<B1-Leave>", lambda event: "break")

        #create a save HTML template  button
        self.saveHTMLButton = tk.Button(self.keyframe,text="HTML Template...",width=15,height=1)
        self.saveHTMLButton.pack()
        self.saveHTMLButton.bind("<Button-1>", self.on_select_saveHTMLButton )       
        

        self.saveFrame = tk.Frame(self.keyframe, width=15)
        self.saveFrame.pack(side=tk.RIGHT, fill=tk.Y)
        #create a save button
        self.saveButton = tk.Button(self.saveFrame,text="SAVE", state=initialSaveButtonState, width=6,height=1)
        self.saveButton.pack(side=tk.LEFT)
        self.saveButton.bind("<Button-1>", self.on_select_saveButton )

        #create a save as button
        self.saveAsButton = tk.Button(self.saveFrame,text="SAVE AS...",width=6,height=1)
        self.saveAsButton.pack(side=tk.RIGHT)
        self.saveAsButton.bind("<Button-1>", self.on_select_saveAsButton )
        

        
        
		
        for item in keys:
            self.listtypes.insert(tk.END, item)
            

        #for item in ["KEY_A" ,       "KEY_NUMLOCK",  "KEY_KPSLASH",  "KEY_KPASTERISK",  "KEY_BACKSPACE", "KEY_A", 
        #  "KEY_HOME",      "KEY_KP7",      "KEY_KP8",      "KEY_KP9",         "KEY_KPMINUS",   "KEY_A", 
        #  "KEY_END",       "KEY_KP4",      "KEY_KP5",      "KEY_KP6",         "KEY_KPPLUS",    "KEY_A", 
        #  "KEY_PAGEUP",    "KEY_KP1",      "KEY_KP2",      "KEY_KP3",         "KEY_ENTER",     "KEY_A", 
        #  "KEY_PAGEDOWN",  "KEY_KP0",      "KEY_KPCOMMA",  "KEY_KPDOT",       "KEY_ENTER",     "KEY_A"]:
        #    listkeys.insert(tk.END, item)

        #find index for item "KEY"
        defTypeI=0
        for item in self.listtypes.get(0, self.listtypes.size()-1):
            if item=="KEY": 
                break
            defTypeI=defTypeI+1
        else:   #no KEY entries !?!?
            defTypeI=0
            
        self.listtypes.bind("<<ListboxSelect>>", self.on_select_type )
        
        self.listbuttons.bind("<Button-1>", self.on_select_keycode )
        
        
        self.listtypes.select_set(defTypeI) #This only sets focus.
        self.listtypes.event_generate("<<ListboxSelect>>")
        self.listtypes.see(defTypeI)
        
    def on_select_keycode(self, event):
        btype =  self.btntype    
        key = self.listbuttons.get(self.listbuttons.nearest(event.y))
        dnd_start(DropItem(btype + "\n" + key, 0, self.docChanged)  ,event)
        
    def on_select_gridbutton(self, event):
        code = event.widget.cget('text')
        dnd_start(DropItem(code,event.widget, self.docChanged)  ,event)
        
        
        
    def on_select_type(self, event):
        widget = event.widget
        sel=widget.curselection()
        if sel:
            self.btntype = widget.get(sel[0])
            self.listbuttons.delete(0,tk.END)
            for item in keys[self.btntype]:
                self.listbuttons.insert(tk.END, item)
                
                
    def on_select_saveButton(self,event):
        with open(args.filename, 'w') as outfile:
            self.writeCfgToFile( outfile )
            self.saveButton.config(state=tk.DISABLED)
                
                
    def on_select_saveAsButton(self,event):
        outfile =tkFileDialog.asksaveasfile(initialdir = "./",title = "Save Config File", filetypes = (("config files","*.json"),("all files","*.*")))
        if( outfile ):
            self.writeCfgToFile( outfile )
            args.filename = outfile.name
            self.calcWindowTitle()
            
        
        
    def writeCfgToFile( self, outfile):
        pttrn = re.compile(r'\s*(\w+?)\n(\w+)\s*')
        cols=cfg['cols']
        for i in range(0,cfg['rows']):
          for j in range(0,cols):
               b = self.gridButtons[i][j]
               text = b.cget('text')
               m = re.match(pttrn, text)
               code = m.group(1)+"_"+m.group(2)
               cfg['keys'][j + i *cols] = code
               
                   
        json.dump(cfg, outfile)

    def docChanged(self):
        self.saveButton.config(state=tk.NORMAL)

    def calcWindowTitle(self):
        self.root.title("gConfigTouchNumpad - " + args.filename)

    def writeHTMLTemplateToFile(self, outfile):
        pttrn = re.compile(r'\s*(\w+?)\n(\w+)\s*')
        h = ('<html>'
        '<head><style type="text/css">\n'
        'td{ height:10mm; text-align:center} \n'
        'table{width: 100mm; height: 70mm; table-layout: fixed;word-wrap: break-word;font-size:2mm;} \n' 
        '</style>  </head>\n'     
        '<body><p>\n'
        '<table border="1" cellpadding="0" cellspacing="0" >'
        '<tbody>\n') 
        for i in range(0,cfg['rows']):
          h +=' <tr> \n' 
          for j in range(0,cfg['cols']):
               b = self.gridButtons[i][j]
               text = b.cget('text')
               m = re.match(pttrn, text)
               h+='<td>'+m.group(2)+'</td>\n'      
          h+=' </tr>\n'
        h+='</table>\n'
        h+='<p>\n'
        h+='&nbsp;</p>\n'
        h+='</body>\n'
        h+='</html>\n'
        outfile.write(h)


    def on_select_saveHTMLButton(self,event):
        outfile =tkFileDialog.asksaveasfile(initialdir = "./",title = "Save HTML Template File", filetypes = (("html","*.html"),("htm","*.htm"),("all files","*.*")))
        if( outfile ):
            self.writeHTMLTemplateToFile( outfile )


if __name__ == "__main__":
    root = tk.Tk()  
    Main(root,cfg).pack(fill="both", expand=True)
    root.mainloop()
